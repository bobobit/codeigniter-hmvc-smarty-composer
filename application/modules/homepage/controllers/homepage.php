<?php if (! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Homepage controller
 * 
 * @package: Core
 * @module: Homepage
 * @created: 01/2014
 * @description: This file handles the homepage's main controller methods.
 * 
 * @author: José Postiga <jose.postiga@josepostiga.com>
 *          Contact me if you have any questions :)
 * 
 */
class Homepage extends MX_Controller
{
    
    /*
     * Construct method
     * 
     * @author: José Postiga <jose.postiga@josepostiga.com>
     * @date: 01/2014
     * 
     * @description: Deals with class' specific var inits
     * 
     */
    function __construct()
    {
        // inerits it's parent init flow
        parent::__construct();
        
        // loads module specific language
        $this->lang->load('homepage');
    }
    
	/*
	 * Index method
	 * 
     * @author: Ellislab team
     * 
	 * @description: Maps to the following URL
	 * 		/index.php/homepage
	 *	- or -  
	 * 		/homepage/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://your-project-domain.ext/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /homepage/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        // Smarty needs the parse method to be used, instead of 
        // $this->load->view()
		$this->parser->parse('index');
	}
    
    /*
     * Hello method
     * 
     * @author: José Postiga <jose.postiga@josepostiga.com>
     * @date: 2013
     * 
     * @description: Shows how to use every optional param in parse function.
     * Pay special attention to $caching param as it sets the Smarty caching 
     * system on/off. Default is FALSE (off).
     * 
     */
    public function hello($name = '')
    {
        // get's the var
        $data['name'] = $name;
        
        $this->parser->parse('index', $data, $return = FALSE, $caching = FALSE, 
                $theme = '');
    }
}